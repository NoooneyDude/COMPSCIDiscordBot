<!-- https://gist.githubusercontent.com/PurpleBooth/109311bb0361f32d87a2/raw/8254b53ab8dcb18afc64287aaddd9e5b6059f880/README-Template.md -->

# COMPSCIDiscordBot

A Discord bot for the UoA COMPSCI server that allows users to subscribe to channels they're interested in The bot also tracks invites to determine which classses to subscribe a newcomer to.

The bot is written in Javascript via node.js and uses the discord.js wrapper for making calls to the Discord API.

### Prerequisites

The following prerequisite is required and is automatically fetched when 'npm install' is run:

```
discord.js
```

## Usage

### Bot Commands

```
help - displays the bot help message.
```
```
list [stage number] - lists all available classes or all classes at the given stage.
```
```
show - shows classes you're subscribed to.
```
```
join [class] <class2> <classN>
```
```
leave [class] <class2> <classN>
```

### Admin-only Commands

```
newclass <class> [class2] [classN] - sets up a new channel role, and creates an invite.
```
```
newinvite <class> [class2] [classN] - creates an invite link with the given class subscriptions.
```
```
showinvites - shows all invites with class subscriptions on join.
```

## Authors

See the list of [contributors](https://gitlab.com/NoooneyDude/COMPSCIDiscordBot/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgements

* Hat tip to benji7425.