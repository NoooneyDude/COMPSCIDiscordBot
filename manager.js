// manager.js: Entry point for COMPSCIDiscordBot project.
// Listens out for commands with a specific prefix,
// loads the relevant command module and then 
// attempts to execute that command.

const fs = require("fs");
const path = require("path");
const Discord = require("discord.js");
const utils = require("./src/util.js");
const winston = require("winston");
const config = require("./config/config.json");
const auth = require("./config/auth.json");

const myFormat = winston.format.printf(({ timestamp, level, message, label }) => {
    return `${timestamp} [${level}]: ${message}`;
});
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        myFormat
      ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: "manager.log" })
    ]
});

class ClassManager {
    constructor(client) {
        this.client = client;
        this.guild = client.guilds.cache.get(config.guildID);
        this.adminRole = this.guild.roles.cache.get(config.adminRoleID);
        this.publicBotChannel = this.guild.channels.cache.get(config.publicBotChannelID);
        this.privateBotChannel = this.guild.channels.cache.get(config.privateBotChannelID);
        this.prefix = config.prefix;
        this.domains = config.domains;
    }
}

const client = new Discord.Client();
const mentionRegEx = new RegExp("^<@!?~?\\d+>(\\s*\\+)?", "g");
let manager;

client.on("ready", () => {
    manager = new ClassManager(client);

    manager.client.user.setActivity("for @Class Manager help", { type: "WATCHING" });
    logger.info("Class Manager started with prefix '" + manager.prefix + "'.");
    manager.privateBotChannel.send(manager.client.user + " started with prefix `" + manager.prefix + "`.");
});

// Runs with every seen message.
client.on("message", async message => {
    if (message.author.bot) return; // Ignore messages from other bots
    if (message.guild && message.guild !== manager.guild) return; // Ignore messages from other guilds

    message.member = manager.guild.members.cache.get(message.author.id);
    if (message.member == null) {
        // Why the member would be null beats me.
        // message.channel.send("You do not appear to be a member of the Discord server `" + manager.guild.name + "`.");
        message.channel.send("Sorry, there was a problem. Contact the admin.");
        message.privateBotChannel.send(manager.adminRole + "Convert it to fetch if not in the cache, you lazy bastard.");
        return;
    }

    let args;
    if (message.mentions.has(client.user) && message.content.match(mentionRegEx)) { // If `@Class Manager <command>`
        args = message.content.replace(mentionRegEx, "");
    }
    else if (message.content.indexOf(config.prefix) == 0) { // If `(prefix)command`
        args = message.content.slice(config.prefix.length);
    }
    else return;

    args = args.trim().split(/[\n\r\s]+/g);
    let issuedCommand = args.shift().toLowerCase();

    if (!issuedCommand) {
        message.channel.send("Need something, " + message.member + "? Try `" + manager.prefix + "help`!"); // TEST
        return;
    }

    // Load and run command from file.
    message.channel.startTyping();
    commandPath = "./src/commands/" + path.basename(issuedCommand) + ".js";
    if (!fs.existsSync(commandPath)) {
        message.react("⚠");
        message.reply("the command '" + issuedCommand + "' doesn't exist. Try `" + manager.prefix + "help`.");
        message.channel.stopTyping(true);
        return;
    }

    try {
        let commandFile = require(commandPath);

        let channelID = message.channel.id;
        if (!message.channel.name) channelID = "direct-message";
        // If user issued a command from a channel that is not allowed:
        // The second condition doesn't currently check to see that the channel issuer is actually a class channel.
        if (!commandFile.allowedChannels.includes(channelID) && !commandFile.allowedChannels.includes("classes")) {
            message.react("🚫");
            message.reply("you cannot issue that command in this channel.");
            message.channel.stopTyping(true);
            return;
        }

        let memberRoleIDs = message.member.roles.cache.map(role => role.id);
        // If user does not have a role that is defined as allowed in command file:
        if (!memberRoleIDs.some(role => commandFile.allowedRoles.includes(role))) {
            message.react("🚫");
            message.reply("you do not have access to this command.");
            message.channel.stopTyping(true);
            return;
        }

        await commandFile.run(manager, message, args);
        // TODO The command module may have deleted the original message.
        message.react("✅");
        message.channel.stopTyping(true);
    }
    catch (err) {
        console.error(err);
        manager.privateBotChannel.send(manager.adminRole +
            ": encountered an `Error`:" +
            "```" + String(err).substring(0, 1900) + "```"); // max msg size 2k chars
        message.react("⚠");
        message.reply("unable to run the command `" +  issuedCommand + "`.");
        message.channel.stopTyping(true);
    }
});

// When a new user joins, check which invites were incremented and assign roles accordingly.
client.on("guildMemberAdd", async newMember => {
    if (newMember.user.bot) return;
    utils.assignRoles(newMember);
});

client.login(auth.token);

client.on('error', err => { // Emitted whenever the client's WebSocket encounters a connection error.
    console.error(err);
    logger.warn("Encountered a WebSocket Connection Error: ", err);
});
client.on('disconnect', event => { // Emitted when the client's WebSocket disconnects and will no longer attempt to reconnect.
    console.error(event);
    logger.error("WebSocket reconnection timed out. Terminating process. ", event);
    process.exit(1);
});

process.on('unhandledRejection', (reason, p) => {
    console.error(reason);
    manager.privateBotChannel.send(manager.adminRole +
        ": encountered an `Unhandled Promise Rejection`:" +
        "```" + String(reason).substring(0, 1900) + "```"); // max msg size 2k chars
});
process.on('uncaughtException', (err, origin) => {
    console.error(err);
    manager.privateBotChannel.send(manager.adminRole +
        ": encountered an `Uncaught Exception`:" + 
        "```" + err.message + "```");
        process.exit(1);
        // Kill the process as per the docs recommendation for unknown state.
        // The environment should reboot the bot.
});