const iData = require("./invites.js");

exports.assignRoles = async (member, debug = false) => {
    let storedInvites = iData.getInvites();
    let guildInvites = await member.guild.fetchInvites();

    for (var inviteCode in storedInvites) {
        var inviteID = storedInvites[inviteCode];
        var inviteHandle = guildInvites.get(inviteCode);
        if (!inviteHandle) {
            iData.remove(inviteCode);
            continue;
        }

        var diff = inviteHandle.uses - inviteID["usages"];
        if (!inviteHandle || !(diff >= 1 || debug)) {
            if (!inviteHandle) {
                iData.remove(inviteCode);
            }
            continue;
        }

        for (var roleIndex = 0; roleIndex < inviteID["roles"].length; roleIndex++) {
            var roleID = inviteID["roles"][roleIndex];
            var role = member.guild.roles.get(roleID);

            if (!role) {
                iData.removeRoleFrom(inviteCode, roleID);
                roleIndex--;
                continue;
            }

            member.addRole(roleID);
        }

        iData.setUsages(inviteCode, inviteHandle.uses);
    }
}
