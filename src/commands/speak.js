// speak.js: Contains module for command 'speak'.
// The bot will state the hardcoded messsage in the hardcoded channel.

exports.allowedRoles = ["458501755265613826"];
exports.allowedChannels = [];
exports.help = "`speak` - states the hardcoded message in the hardcoded channel.";

exports.run = async (manager, message, args) => {
    manager.guild.channels.cache.find(channel => channel.name == "welcome").send(
        "__**Welcome to the University of Auckland COMPSCI Discord Server!**__"
        + "\n\nTo get started, you'll want to `subscribe to some classes` - by default, channels for individual classes are hidden."
        + " This is to reduce clutter in the server."
        + " You may already be subscribed to some classes. This probably because you used a special invite link."
        + "\n\nSubscribe to the classes you're interested in by heading over to the <#458564385649197056> channel."
        + "\nOver there, our <@459211651149987852> bot will listen out for your `+join <class>` command."
        + "\nYou can get further help by using the command `+help`."
        + "\n\n_Remember, all commands must be prefixed by a `+`! Alternatively, mention <@459211651149987852>._"
    );
};
