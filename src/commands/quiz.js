// quiz.js: Contains module for command 'quiz'.
// Sends a message to the channel with the given message, and adds A, B, C, D, E reactions, for voting.

// TODO Tick reaction in manager.js may beat or lag the message deletion, causing promise rejection.

exports.allowedRoles = ["458498248479342606"];
exports.allowedChannels = ["459561722454343680", "classes"];
exports.help = "`quiz <question>` - displays the question and adds A, B, C, D, E reactions.";

exports.run = (manager, message, args) => {
    let question = args.join(' ');
    if (question) {
        message.channel.send("```" + question + "```")
        .then(message.delete())
        .then(function (quiz) {
            quiz.react('🇦')
            .then(() => quiz.react('🇧'))
            .then(() => quiz.react('🇨'))
            .then(() => quiz.react('🇩'))
            .then(() => quiz.react('🇪'))
            .catch(() => console.error("Couldn't post the quiz reactions."));
        })
        .catch(() => console.error("Couldn't post the quiz."));
    }
    else {
        message.reply("cannot post an empty quiz.");
    }
}