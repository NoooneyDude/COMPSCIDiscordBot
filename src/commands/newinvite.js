// newinvite.js: Contains module for command 'newinvite'.
// Allows an administrator to create a new invite link with 
// the given class subscriptions.

const iData = require("../invites.js");

exports.allowedRoles = ["458501755265613826"];
exports.allowedChannels = ["direct-message", "459561722454343680"];
exports.help = "`newinvite <class> [class2] [classN]` - creates an invite link with the given class subscriptions.";

exports.run = async (manager, message, args) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.

    let invite = await message.guild.channels.cache.find(channel => channel.name == "welcome").createInvite( {
        maxAge: 0,
        unique: true
    });

    for (let i = 0; i < args.length; i++) {
        let role = message.guild.roles.cache.find(role => role.name == args[i]);
        if (role) iData.addRoleFor(invite.code, role.id);
        else message.reply(args[i] + " is not a valid class. It will not be included in the invite.");
    }
    message.author.sendMessage("Here is the invite link with the given class subscriptions: " + invite.url);
    message.reply("you've been sent a direct message.");
}
