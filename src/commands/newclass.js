// newclass.js: Contains module for command 'newclass'.
// Allows an administrator to create the given class(es).
// Sets up a new role, channel, and creates an invite for each class.

const iData = require("../invites.js");

exports.allowedRoles = ["458501755265613826"];
exports.allowedChannels = ["direct-message", "459561722454343680"];
exports.help = "`newclass <class> [class2] [classN]` - sets up a new channel, role, and creates an invite.";

exports.run = async (manager, message, args) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.
    const guild = manager.guild;
    const defaultRole = guild.roles.cache.find(role => role.name == "@everyone");
    const modRole = guild.roles.cache.find(role => role.name == "Moderator"); // TODO hardcoded names (not IDs) bad
    const botRole = guild.roles.cache.find(role => role.name == "Class Manager");

    for (i = 0; i < args.length; i++) {
        let className = args[i];
        let categoryName = className.replace('-', ' ');

        let role = guild.roles.cache.find(role => role.name == className);
        if (!role) role = await guild.roles.create({ data: { name: className } });
        await role.edit({ // Actually, should be okay to async this..
            //mentionable: true,    // If enabled, allows every user to mention the role. Not good! Class Reps+ can use @everyone (in respective channel) instead.
            permissions: 103926849  // https://i.imgur.com/dCp0YhX.png
        });

        let categoryChannel = guild.channels.cache.find(channel => channel.name == categoryName && channel.type == "category");
        if (!categoryChannel) categoryChannel = await guild.channels.create(categoryName, { type: "category" });
        categoryChannel.updateOverwrite(role, {"VIEW_CHANNEL": true });
        //categoryChannel.updateOverwrite(modRole, {"VIEW_CHANNEL": true }); // Mod role perms revoked; mods didn't want to see all channels
        await categoryChannel.updateOverwrite(botRole, {"VIEW_CHANNEL": true }); // we need to wait, because bot will need access shortly
        categoryChannel.updateOverwrite(defaultRole, {"VIEW_CHANNEL": false });

        let textChannel = categoryChannel.children.find(channel => channel.name == className);
        if (!textChannel) textChannel = await guild.channels.create(className, { type: "text" });
        textChannel.setParent(categoryChannel);
        textChannel.updateOverwrite(role, {"VIEW_CHANNEL": true });
        //textChannel.updateOverwrite(modRole, {"VIEW_CHANNEL": true });
        await textChannel.updateOverwrite(botRole, {"VIEW_CHANNEL": true }); // we need to wait, because bot will need access shortly
        textChannel.updateOverwrite(defaultRole, {"VIEW_CHANNEL": false});

        let voiceChannel = categoryChannel.children.find(channel => channel.name == "voice");
        if (!voiceChannel) voiceChannel = await guild.channels.create("voice", { type: "voice" });
        voiceChannel.setParent(categoryChannel);
        voiceChannel.updateOverwrite(role, {"VIEW_CHANNEL": true });
        //voiceChannel.overwritePermissions(modRole, {"VIEW_CHANNEL": true });
        voiceChannel.updateOverwrite(botRole, {"VIEW_CHANNEL": true });
        voiceChannel.updateOverwrite(defaultRole, {"VIEW_CHANNEL": false});

        let invite = await guild.fetchInvites()
        invite = invite.find(invite => invite.channel == textChannel);
        if (!invite) invite = await textChannel.createInvite({ maxAge: 0, unique: true });
        if (!iData.getInvites()[invite.code]) {
            iData.addRoleFor(invite.code, role.id);
        }

        let pinnedMessages = await textChannel.messages.fetchPinned();
        pinnedMessages = pinnedMessages.filter(message => message.author == client.user);
        if (pinnedMessages.size === 0) {
            textChannel.send("Invite link to this class: " + invite.url)
                .then(sentMessage => sentMessage.pin());
            continue;
        }

        pinnedMessages.forEach(message => {
            if (message.content.startsWith("Invite link to this class: ")) {
                message.edit("Invite link to this class: " + invite.url + 
                    "people that join using this link will automatically have the correct role to see this class.")
            }
        });
    }

    let channelPositions = [];  // role.name.substring(0, role.name.indexOf('-'))))
    let channels = manager.guild.channels.cache.array().filter(channel => 
        manager.domains.includes(channel.name.substring(0, channel.name.indexOf('-'))));
    channels.sort((a, b) => a.name.localeCompare(b.name));

    channels.forEach((channel, index) => {
        channelPositions.push({ channel: channel, position: index });
    });
    guild.setChannelPositions(channelPositions);

    message.reply("the roles, channels and invites for each specified class have been created!\n" +
        "The invite links have been posted and pinned to each class channel.");
};
