// leave.js: Contains module for command 'leave'.
// Allows a user to unsubscribe from the given class(es).

exports.allowedRoles = ["458498248479342606"];
exports.allowedChannels = ["direct-message", "459561722454343680", "458564385649197056"];
exports.help = "`leave <class> [class2] [classN]` - leaves the given class(es).";

exports.run = async (manager, message, args) => {
    if (args.length < 1) {
        message.channel.send("Usage: " + exports.help);
        return;
    }

    args = [...(new Set(args))]; // Remove duplicates.
    let member = message.member;
    let response = "";

    for (let i = 0; i < args.length; i++) {  
        let role = manager.guild.roles.cache.find(role => role.name == args[i]);
        
        if (!role || !manager.domains.includes(role.name.substring(0, role.name.indexOf('-')))) {
            response += "- the class '" + args[i] + "' doesn't appear to exist. See `show`.\n";
            continue;
        }

        let channel = manager.guild.channels.cache.find(channel => channel.name == args[i]);
        let channelName = channel ? channel.toString() : ("`" + role.name + "`");

        if (!member.roles.cache.has(role.id)) {
            response += "- you're not subscribed to " + channelName + ". See `show`.\n";
            continue;
        }
    
        await member.roles.remove(role)
        .then(function() {
            response += "- you're now unsubscribed from " + channelName + ".\n";
        })
        .catch(function() {
            response += "- couldn't unsubscribe you from " + channelName + ".\n";
        });
    }
    
    message.reply("\n" + response);
}
