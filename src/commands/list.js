// list.js: Contains module for command 'list'.
// Displays a list of the classes available.

exports.allowedRoles = ["458498248479342606"];
exports.allowedChannels = ["direct-message", "459561722454343680", "458564385649197056"];
exports.help = "`list [department]` - lists all available classes or all classes in the given department.";

exports.run = (manager, message, args) => {
    let textChannels = manager.guild.channels.cache.array();
    textChannels = textChannels.filter(channel => {
        if (channel.type != "text") return false; //return (channel);
        if (manager.domains.includes(channel.name.substring(0, channel.name.indexOf('-')))) return true;
    });

    if (textChannels.length == 0) {
        message.channel.send("There are no available classes.");
        return;
    }

    textChannels.sort((a, b) => a.name.localeCompare(b.name));

    let responseMessage = "__**Available " + (textChannels.length == 1 ? "class:" : "Classes") + "**__\n";
    
    if (textChannels.length == 1) {
        responseMessage += textChannels.map(channel => channel.name).join("\n");
        message.channel.send(responseMessage);
        return;
    }

    if (args[0]) {
        if (manager.domains.includes(args[0])) {
            responseMessage += getClassesString(textChannels, args[0]);
        }
        else {
            message.reply("couldn't find that faculty.");
            return;
        }
    }
    else {
        manager.domains.forEach(function (domain) {
            responseMessage += getClassesString(textChannels, domain);
        });
    }

    if (message.channel.name) message.reply("you've been sent a direct message.");
    message.author.send(responseMessage);
}

function getClassesString(channels, domain) {
    channels = channels.filter(channel => {
        return (channel.name.substring(0, channel.name.indexOf('-')) == domain)
    });

    let stage1 = channels.filter(channel => {
        return (channel.name[channel.name.indexOf('-') + 1] == 1);
    });

    let stage2 = channels.filter(channel => {
        return (channel.name[channel.name.indexOf('-') + 1] == 2);
    });

    let stage3 = channels.filter(channel => {
        return (channel.name[channel.name.indexOf('-') + 1] == 3);
    });

    stage1 = stage1.map(channel => '`' + channel.name + '`').join("\n");
    stage2 = stage2.map(channel => '`' + channel.name + '`').join("\n");
    stage3 = stage3.map(channel => '`' + channel.name + '`').join("\n");

    return "\n**" + domain.toUpperCase() + " Classes:**\n" +
        "Stage 1:\n" + stage1 + "\nStage 2:\n" + stage2 + "\nStage 3:\n" + stage3;
}