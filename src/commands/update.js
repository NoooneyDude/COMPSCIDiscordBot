// updatejs: Contains module for command 'update'.
// Allows an administrator to update class(es).
// Sets up a new role, channel, and creates an invite for each class...

exports.allowedRoles = ["458501755265613826"];
exports.allowedChannels = ["direct-message", "459561722454343680"];
exports.help = "`update` - checks with UoA API to ensure classes are up to date.";

exports.run = async (manager, message, args) => {    
    const https = require('https');
    // perhaps if month is after around november, advance by one year.
    let year = (new Date()).getFullYear();
    let subject = "COMPSCI";
    let url = "https://api.auckland.ac.nz/service/courses/v2/courses?subject=" + subject + "&year=" + year + "&size=500";
    //let options = {
    //    json: true
    //}

    https.get(url, (res) => {
        let body = "";

        res.on('data', (d) => {
            body += d;
        });

        res.on('end', function() {
            courseData = JSON.parse(body);
            courseCount = courseData.total;
            courses = courseData.data;
            actualCourses = [];

            for (let i in courses) {
                let course = courses[i];
                let acceptedStages = [1, 2, 3];
                if ("123".indexOf(course.catalogNbr[0]) == -1) continue;

                actualCourses.push(course);
                console.log(course.subject + " " + course.catalogNbr);
            }

            message.reply("Number of courses available: " + actualCourses.length);
        });
    
    }).on('error', (e) => {
        console.error(e);
    });
}
