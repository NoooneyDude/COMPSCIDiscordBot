// show.js: Contains module for command 'show'.
// Displays a list of classes the user is subscribed to.

exports.allowedRoles = ["458498248479342606"];
exports.allowedChannels = ["direct-message", "459561722454343680", "458564385649197056"];
exports.help = "`show` - shows classes you're subscribed to.";

exports.run = (manager, message, args) => {
    let roles = message.member.roles.cache.array();
    roles = roles.filter(role => manager.domains.includes(role.name.substring(0, role.name.indexOf('-'))));

    if (roles.length == 0) {
        message.reply("you're not currently subscribed to any classes.");
        return;
    }

    let responseMessage = "you're currently subscribed to " + roles.length + " " +
        (roles.length > 1 ?  "classes:" : "class:") + "\n";

    roles.forEach(role => {
        let channel = manager.guild.channels.cache.find(channel => channel.name == role.name && channel.type == "text");
        responseMessage += (channel ? channel.toString() : ("`" + role.name + "`")) + "\n"; // If the role exists but not the channel (shouldn't happen)
    });

    message.reply(responseMessage);
}
