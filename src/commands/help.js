// help.js: Contains module for command 'help'.
// Sends the summoner a direct message with a list of available commands.

const fs = require("fs");

exports.allowedRoles = ["458498248479342606"];
exports.allowedChannels = ["direct-message", "459561722454343680", "458564385649197056"];
exports.help = "`help` - displays this help message.";

exports.run = (manager, message, args) => {
    let member = manager.guild.members.cache.get(message.author.id);
    let botCommandsChannel = manager.guild.channels.cache.find(channel => channel.name == "bot-commands");

    fs.readdir("./src/commands/", (err, files) => {
        let helpLine = 
            "__**Class Manager Help**__"
            + "\nTo execute a command, you must prefix commands with either " + manager.client.user + ", or `" + manager.prefix + "`."
            + "\nSome commands must only be issued in the " + botCommandsChannel + " channel."
            + "\n`<>` arguments are required, while `[]` arguments are optional."
            + "\nFor example, `" + manager.prefix + "list compsci` would list all available COMPSCI classes.\n\n";

        if (files.length == 0) {
            message.channel.send("**There are no available commands.**");
            return;
        }

        helpLine += "**Available " + (files.length == 1 ? "command" : "commands") + ":**\n";

        files.forEach(file => {
            try {
                let command = require("./" + file);
                let userRoleNames = member.roles.cache.map(role => role.id);

                // If user has a role that is defined as allowed in command file:
                if (userRoleNames.some(role => command.allowedRoles.includes(role)))
                    helpLine += command.help + "\n";
            }
            catch (err) {
                console.error(err);
            }
        });

        helpLine += "\nYou can check out the bot's `GitLab Repo` here: <https://gitlab.com/NoooneyDude/COMPSCIDiscordBot>.";

        if (message.channel.name) message.reply("you've been sent a direct message.");
        message.author.send(helpLine);
    });
}
